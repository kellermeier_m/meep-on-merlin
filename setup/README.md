```bash
# After extracting all tarballs go to each directory seperately to install it
# in libunistring dir:
./configure --prefix=$HOME/install
make install
# In libffi dir:
./configure --prefix=$HOME/install
Make install
# In libatomic_ops dir:
./configure --prefix=$HOME/install
Make install
# In gc dir (resp. Bdw-gc):
./configure --prefix=$HOME/install
Make install
# In guile dir (resp. Bdw-gc):
# exports taken from `install_meep.sh`
export LIBFFI_CFLAGS="-I$HOME/install/lib/libffi-3.0.11/include"
export LIBFFI_LIBS="-L$HOME/install/lib -lffi -L$HOME/install/lib64 -lffi"
export ATOMIC_OPS_CFLAGS="-I$HOME/install/include -I$HOME/install/include/atomic_ops"
export ATOMIC_OPS_LIBS="-L$HOME/install/lib"
export BDW_GC_CFLAGS="-I$HOME/install/include -I$HOME/install/include/gc"
export BDW_GC_LIBS="-L$HOME/install/lib -lgc"
./configure --prefix=$HOME/install LDFLAGS="-L$HOME/install/lib -L$HOME/install/lib64" CPPFLAGS="-I$HOME/install/include -I$HOME/install/lib/libffi-3.2.1/include/"
make   # gives some warnings
make install
# -> Guile is installed

# In libctl dir: 
./configure --prefix=$HOME/install
Make install

# In harminv dir:
# Make sure that BLAS was found: checking for sgemm_ in -lblas... Yes   or similar when configuring
# Probably not found due to wrong setting of ENV variables LDFLAGS resp. CPPFLAGS
./configure --prefix=$HOME/install
Make install

#In h5utils dir:
./configure --prefix=$HOME/install
Make install
	
# OPTIONAL: MPB
# In fftw dir: 
./configure --prefix=$HOME/install --enable-mpi
make install
Make
# In MPB dir:
./configure --prefix=$HOME/install --with-mpi --with-libctl=$HOME/install/share/libctl LDFLAGS="-L$HOME/install/lib"
make
make install
make distclean
# install MPB with inversion symmetry
./configure --prefix=$HOME/install --with-mpi --with-inv-symmetry --with-libctl=$HOME/install/share/libctl LDFLAGS="-L$HOME/install/lib"
make install
make distclean
		
# does not work
./configure --prefix=$HOME/install --with-libctl=$HOME/install/share/libctl LDFLAGS="-L/opt/gcc/gcc-4.6.3/lib64/ -L$HOME/install/lib" LIBS="-lgfortran"  

# In Meep dir:
export CPPFLAGS="-I$HOME/install/include"
export LDFLAGS="-L$HOME/install/lib"
./configure --prefix=$HOME/install --with-mpi --with-libctl=$HOME/install/share/libctl CPPFLAGS="-I$HOME/install/include" LDFLAGS="-L$HOME/install/lib"
```