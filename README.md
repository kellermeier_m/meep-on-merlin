Running MEEP on Merlin cluster
===

This is a repository to setup and MEEP and MPB on the Merlin cluster.
The main repos [MEEP](https://gitlab.psi.ch/kellermeier_m/MEEP) and [MPB](https://gitlab.psi.ch/kellermeier_m/MPB) contain the control files for the simulations.


Setup
===
Before installation the compiler and provided modules have to be loaded on the cluster. The [Pmodule](https://amas.psi.ch/Pmodules/wiki/Pmodules?redirectedfrom) system was used which can be activated by the following. Since in the hierarchy tree of `gcc 5.3.0` no stable hdf5 module is currently available unstable modules were activated. Other gcc versions and corresponding hierarchy trees were not tested.

```
# used for pmodule system
if [[ -r "/opt/psi/config/profile.bash" ]]; then
source "/opt/psi/config/profile.bash"
fi

#module load gcc/5.3.0 OpenBLAS/0.2.19 openmpi/1.10.2 hdf5/1.8.17
module use unstable
module load gcc/5.4.0 openmpi/1.10.4 OpenBLAS/0.2.19 hdf5/1.8.17
```


The folder `setup` contains the following files:
- `wget_list.txt`
- `tar_list.txt`
- `README.md`

The README contains the install instructions in a summarized way. Commands like 
`cd` to change to a directory are ommitted.
In `wget_list.txt` all resources needed to install it are listed with the download command. 
The links refer to specific versions of each library/application. Only this combination 
of versions was tested so far.
`tar_list.txt` contains the unpacking commands for the downloaded files. It assumes
that the downloaded files are located in a subdirectory `tarballs`. In my case this 
directory was located at `~/Download/tarballs` and the files were unpacked to 
`~/Download`.

Following the instructions in `README.md` installs the libraries according to their
dependencies.

Preparing simulations
===
The control files for MEEP resp. MPB are provided in the mentioned separate repositories.
Those control files (`*.ctl*`) are copied to the Merlin cluster either via `scp` or
by cloning the repositories.

Running simulations
===
In the *template* directory the [**run.sge**](/template/run.sge) provides a submission script for the simulations.
Some details on how to proceed are given in the README in this directory.



