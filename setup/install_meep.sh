export LD_LIBRARY_PATH="$HOME/install/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH="$HOME/install/lib64:$LD_LIBRARY_PATH"

# those are needed for installing guile
export LIBFFI_CFLAGS="-I$HOME/install/lib/libffi-3.0.11/include"
export LIBFFI_LIBS="-L$HOME/install/lib -lffi -L$HOME/install/lib64 -lffi"
export ATOMIC_OPS_CFLAGS="-I$HOME/install/include -I$HOME/install/include/atomic_ops"
export ATOMIC_OPS_LIBS="-L$HOME/install/lib"
export BDW_GC_CFLAGS="-I$HOME/install/include -I$HOME/install/include/gc" 
export BDW_GC_LIBS="-L$HOME/install/lib -lgc"



