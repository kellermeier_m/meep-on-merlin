rm *.png
rm *.pyc
rm *~
rm *.npy
rm video/topview/*.png
rm video/phasespace/*.png
rm *.dump
rm -r diags
rm core.*
rm *.e*
rm *.o*
rm *.pe*
rm *.po*
rm configuration.txt
