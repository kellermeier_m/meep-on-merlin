README


1. Import the right gcc module and the matching openblas, openmpi and hdf5 modules:
```bash
# used for pmodule system
if [[ -r "/opt/psi/config/profile.bash" ]]; then
source "/opt/psi/config/profile.bash"
fi
# Load the environment modules for this job (the order may be important):
module add gcc/5.4.0
module add openmpi/1.10.4
module add OpenBLAS/0.2.19
module add hdf5/1.8.17
```
  For keeping this module combination this loading can be kept in `.bashrc` or
  similar.
  For the simulation this module loading is not needed since it is done in the
  run.sge script anyways.

2. Set the simulation parameters a desired:
  - Initial simulation parameters are defined in the control file '*.ctl'
  - The can be modified per simulation via command line arguments, see `ARGS` in
  the next section.

3. Run the simulation:
  - set number of cores in run.sge:
    * line 4: `#$ -pe orte 128  (for 128 cores)`
    * line 30: ` ARGS=' excite-mounted_2D_wvg.ctl' `
	* provide command line arguments for example via
	line 30: ` ARGS=' initial-src-center?=true fcen=0.47 excite-mounted_2D_wvg.ctl' `
  - commit job to merlin cluster:
	qsub run.sge
  - look at progress using:
	tail -f MEEP-Pisa-structure.o*

4. Output of the simulation:
  - most output in the MEEP-Pisa-structure.o* comes from setting up the simulation 
  environment. The output from MEEP starts roughly at line 215. To print part of
  it use:
  ```
  sed -n 215,260p  ~/simulations/MEEP-Pisa-structure.o
  ```

